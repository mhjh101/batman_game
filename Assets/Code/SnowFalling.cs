﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SnowFalling : MonoBehaviour
{
    public UnityEvent onTriggerEnter;
    public static bool InHouse = false;

    public GameObject Snow;
   

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Inside();
        } else
        {
            Outside();
        }
       
    }
    void Update()
    {
       
    }
    void Inside()
    {
        Snow.SetActive(false);
        
    }
    void Outside()
    {
        Snow.SetActive(true);
       

    }

    //public void SnowTrigger()
    //{
    //    if (InHouse)
    //    {
    //        StartCoroutine(SnowEvent());
    //    }
    //}
    //IEnumerator SnowEvent()
    //{
    //    InHouse = false;

    //    _anim.SetTrigger("Inside");

        
    //}
}
