﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;

    public GameObject PopUpMenuUI;
    
    void Start()
    {
        
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (GameIsPaused)
            {
                Resume();
            } else
            {
                Paused();
            }
        }
    }
    void Resume()
    {
        PopUpMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }
    void Paused()
    {
        PopUpMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;

    }
}
