﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCNTRL : MonoBehaviour
{ 

    public float mouseSensitivity = 100f;

    public Transform playerbody;

    float xRotation = 0f;

    public bool canAcceptInput = false;
    public int AnimationDelay;

    
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        //StartGame();

}

   public void StartGame()
    {
       
        //StartCoroutine(TimeRoutine(5));


    }
    //IEnumerator TimeRoutine(int _start)
    //{
    //    //yield return new WaitForSeconds(_start);
    //    //canAcceptInput = true;

    //}
    void Update()
    {
        
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }


        if (canAcceptInput)
        {
            float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90f, 90f);

            transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
            playerbody.Rotate(Vector3.up * mouseX);
        }

    }
}
