﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatFinalAnimation : MonoBehaviour
{
    public AnimationClip batClip;
    public Animator cabinAnimator;
    public AnimationClip bodyClip;
    public Animator bodyAnimator;
    public Transform Player;
    public float animationOffset = 0.1f;
    public AnimationClip EndCreditsAnimator;
    public Animator EndCredits;

    private bool playAnimation = true;



    public Transform lerpPosition;
    public float lerpDuration;
    private bool canLerp = true;

    private bool hasTriggered = false;
    //public bool animationRun = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) && playAnimation)
        {
            StartCoroutine(PlayAfterAnimation01());
        }
    }
    IEnumerator LerpSequence(GameObject player)
    {
        if (lerpPosition != null)
        {

            yield return LerpCharPosition(player.transform, player.transform.position, lerpPosition.position, lerpDuration);

        }
        yield return PlayAfterAnimation01();

        player.GetComponent<NewMovementScript>().UpdateInput(true);
        canLerp = true;

        yield break;
    }
    IEnumerator PlayAfterAnimation01()
    {

        //Play the clip
        cabinAnimator.SetTrigger("Play");
        bodyAnimator.SetTrigger("Play");
        EndCredits.SetTrigger("Play");
        //Wait for the length of the clip + an offset if we want to wait longer

        //Do things here, this is an example to just show that it worked
        //We can animate again
        yield break;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (!hasTriggered)
            {


                (hasTriggered) = true;
                GameObject player = other.gameObject;
                player.GetComponent<NewMovementScript>().UpdateInput(false);
                //animationRun = true;
                //player.GetComponent<Rigidbody>().isKinematic = true;


            }
        }
    }

    IEnumerator LerpCharPosition(Transform _player, Vector3 _startPos, Vector3 _endPos, float _duration)
    {
        float elapsedTime = 0;
        Vector3 startRotation = _player.localEulerAngles;
        while (elapsedTime <= _duration)
        {
            float time = elapsedTime / _duration;
            float xPos = Mathf.Lerp(_startPos.x, _endPos.x, time);
            float zPos = Mathf.Lerp(_startPos.z, _endPos.z, time);

            float yRot = Mathf.LerpAngle(startRotation.y, lerpPosition.localEulerAngles.y, time);
            Vector3 targetPos = new Vector3(xPos, _player.position.y, zPos);
            Vector3 targetRot = new Vector3(_player.localEulerAngles.x, yRot, _player.localEulerAngles.z);

            _player.position = targetPos;
            _player.localEulerAngles = targetRot;

            Debug.Log(targetPos);
            yield return null;
            elapsedTime += Time.deltaTime;
        }

    }
}