﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SnowOnTemp : MonoBehaviour
{
    public UnityEvent onTriggerEnter;
    public static bool InHouse = false;

    public GameObject Snow;
    

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Outside();
        }
        else
        {
            Inside();
        }

    }
    void Update()
    {

    }
    void Inside()
    {
        Snow.SetActive(true);

    }
    void Outside()
    {
        
        Snow.SetActive(false);

    }
}
