﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewMovementScript : MonoBehaviour
{

    //public float movementSpeed = 10;
    //private Rigidbody _rb;

    public CharacterController controller;
    public float speed = 12f;
    public float gravity = -9.81f;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 velocity;

    bool isGrounded;
    bool canAcceptInput = true;

    public CameraCNTRL CameraControls;

    // Start is called before the first frame update
    void Start()
    {
        //_rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        if (canAcceptInput)
        {

        
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
        }

    }
    private void FixedUpdate()
    {
        //float verticalMove = Input.GetAxis("Vertical");
        //float horizontalMove = Input.GetAxis("Horizontal");

        //_rb.velocity = transform.forward * verticalMove * movementSpeed;
    }
    public void UpdateInput(bool _Input)
    {
        CameraControls.canAcceptInput = _Input;
        canAcceptInput = _Input;
    }
}
