﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class CharacterMovement : MonoBehaviour

{
    public GameObject openingTitle;
    public GameObject PopUpMenu;
    public GameObject MenuButton;

    public float speed = 3.0F;
    public float rotateSpeed = 3.0F;

    public bool IsWalking;

    private Animator anim_;
    private int moveInput;

    private void Start()
    {
        openingTitle.SetActive(true);
        MenuButton.SetActive(false);

        anim_ = GetComponent<Animator>();

    }
    private void FixedUpdate()
    {
        
    }
    void Update()
    {
        CharacterController controller = GetComponent<CharacterController>();

        // Rotate around y - axis
        transform.Rotate(0, Input.GetAxis("Horizontal") * rotateSpeed, 0);

        // Move forward / backward
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        float curSpeed = speed * Input.GetAxis("Vertical");
        controller.SimpleMove(forward * curSpeed);
    }
}