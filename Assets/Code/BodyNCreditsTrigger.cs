﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BodyNCreditsTrigger : MonoBehaviour
{
    public UnityEvent onTriggerEnter;
    public Animator BodyAnimator;
    public AnimationClip BodyClip;

    private bool hasTriggered = false;

    // Start is called before the first frame update
    void Start()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            BodyAnimator.SetTrigger("Play");
        }
        
    }
    void Update()
    {


    }
   
}
    // Update is called once per frame
