﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraShutOff : MonoBehaviour
{
    public Camera PlayerCamera;
    public Animator CameraAnimator;

    // Start is called before the first frame update
    void Start()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            CameraAnimator.SetTrigger("Play");
        }

    }
    void Update()
    {


    }
}
