﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEMOLerpAfterAnimation : MonoBehaviour
{
    public AnimationClip animationClip;
    public Animator redCubeAnim;
    public Transform greenCubeTrans;
    public float animationOffset = 0.1f;

    private bool canAnimate = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Press P to animate with the coroutine
        if (Input.GetKeyDown(KeyCode.P) && canAnimate)
        {
            StartCoroutine(PlayAfterAnimation01());
        }
    }

    IEnumerator PlayAfterAnimation01()
    {
        //Prevents us from doing this while it's doing things
        canAnimate = false;
        //Get the length of our clip
        float clipLength = animationClip.length;
        //Play the clip
        redCubeAnim.SetTrigger("Play");
        //Wait for the length of the clip + an offset if we want to wait longer
        yield return new WaitForSeconds(clipLength + animationOffset);
        //Do things here, this is an example to just show that it worked
        greenCubeTrans.position += new Vector3(6, 0, 0);
        //We can animate again!
        canAnimate = true;
        yield break;
    }
}
