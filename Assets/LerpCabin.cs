﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpCabin : MonoBehaviour
{
    public Camera animationCamera;
    public Camera playerCamera;

    public Animator BodyAnimator;

    public AnimationClip animationClip;
    public Animator handAnimator;
    public Transform Player;
    public float animationOffset = 0.1f;

    private bool playAnimation = true;


    public Transform lerpPosition;
    public float lerpDuration;
    private bool canLerp = true;

    private bool hasTriggered = false;
    //public bool animationRun = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) && playAnimation)
        {
            StartCoroutine(PlayAfterAnimation01());
        }
    }
    IEnumerator LerpSequence(GameObject player)
    {
        if (lerpPosition != null)
        {

            yield return LerpCharPosition(player.transform, player.transform.position, lerpPosition.position, lerpDuration);

        }
        yield return PlayAfterAnimation01();

        player.GetComponent<NewMovementScript>().UpdateInput(true);
        canLerp = true;

        yield break;
    }
    IEnumerator PlayAfterAnimation01()
    {
        animationCamera.enabled = true;
        playerCamera.enabled = false;

        Debug.Log(gameObject + "EndCreditsTotal enabled");


        //Get the length of our clip
        //float clipLength = animationClip.length;
        //Play the clip
        handAnimator.SetTrigger("Play");
        BodyAnimator.SetTrigger("Play");
        ////Wait for the length of the clip + an offset if we want to wait longer
        //yield return new WaitForSeconds(clipLength + animationOffset);
        ////Do things here, this is an example to just show that it worked
        ////We can animate again

        //Debug.Log(gameObject + "Has hit Play After Animation");
      

        yield break;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (!hasTriggered)
            {


                (hasTriggered) = true;
                GameObject player = other.gameObject;
                player.GetComponent<NewMovementScript>().UpdateInput(false);
                //animationRun = true;
                //player.GetComponent<Rigidbody>().isKinematic = true;

                //if (canLerp)
                //{
                //    canLerp = false;
                //    StartCoroutine(LerpSequence(player));
                //}
            }
        }
    }
    IEnumerator LerpCameraPosition(Transform animCamera, Transform playerCamera, float duration)
    {
        float elapsedTime = 0;
        Vector3 initRotation = playerCamera.eulerAngles;
        Vector3 targetRotation = animCamera.eulerAngles;

        while (elapsedTime < duration)
        {
            float time = elapsedTime / duration;
            float xRot = Mathf.LerpAngle(initRotation.x, targetRotation.x, time);
            float yRot = Mathf.LerpAngle(initRotation.y, targetRotation.y, time);
            float zRot = Mathf.LerpAngle(initRotation.z, targetRotation.z, time);

            playerCamera.transform.eulerAngles = new Vector3(xRot, yRot, zRot);
            yield return null;

            elapsedTime += Time.deltaTime;
        }
        yield break;
    }

    IEnumerator LerpCharPosition(Transform _player, Vector3 _startPos, Vector3 _endPos, float _duration)
    {
        StartCoroutine(LerpCameraPosition(animationCamera.transform, playerCamera.transform, _duration));
        float elapsedTime = 0;
        Vector3 startRotation = _player.eulerAngles;
        while (elapsedTime <= _duration)
        {
            float time = elapsedTime / _duration;
            float xPos = Mathf.Lerp(_startPos.x, _endPos.x, time);
            float zPos = Mathf.Lerp(_startPos.z, _endPos.z, time);

            float yRot = Mathf.LerpAngle(startRotation.y, lerpPosition.eulerAngles.y, time);
            Vector3 targetPos = new Vector3(xPos, _player.position.y, zPos);
            Vector3 targetRot = new Vector3(_player.eulerAngles.x, yRot, _player.eulerAngles.z);

            _player.position = targetPos;
            _player.eulerAngles = targetRot;

            Debug.Log(targetPos);
            yield return null;
            //elapsedTime += Time.deltaTime;
        }

        yield break;
    }

}
